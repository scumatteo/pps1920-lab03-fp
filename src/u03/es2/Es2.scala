package u03.es2

import u03.Lists.List
import u02.Optionals.Option
import u03.Lists.List._
import u02.Optionals.Option._


object Es2 {

  def maxWithTail(l: List[Int]): Option[Int] = l match {
    case Nil() => None()
    case _ =>
      @scala.annotation.tailrec
      def _maximum(l: List[Int], current: Int) : Option[Int] = l match {
        case Cons(h,t) if h > current => _maximum(t, h)
        case Cons(_,t) => _maximum(t, current)
        case Nil() => Some(current)
      }
      _maximum(l, Int.MinValue)
  }
}
