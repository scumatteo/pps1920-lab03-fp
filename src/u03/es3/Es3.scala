package u03.es3


import u03.Lists.List
import u03.Lists.List._
import u03.es3.Person.Person
import u03.es3.Person.Person._

object Es3 {

  def takeCourses(l: List[Person]): List[String] = map(filter(l) {
    case Teacher(_, _) => true
    case _ => false
  })(e => course(e))

  def takeCoursesFlatMap(l: List[Person]): List[String] = flatMap(l){
    case Teacher(_, course) => Cons(course, Nil())
    case _ => Nil()
  }



}
