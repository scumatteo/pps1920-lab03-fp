package u03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.es3.Person.Person
import u03.es3.Person.Person._
import u03.Lists.List
import u03.Lists.List._
import u03.es3.Es3

class TestEs3 {

  val lst : List[Person] = Cons(Teacher("Mirko", "oop"), Cons(Student("Matteo", 2020), Cons(Teacher("Maltoni", "ML"),
              Cons(Student("Marco", 2020), Nil()))))

  @Test def testEs3(): Unit ={
    assertEquals(Cons("oop", Cons("ML", Nil())), Es3.takeCourses(lst))
    assertEquals(Cons("oop", Cons("ML", Nil())), Es3.takeCoursesFlatMap(lst))
  }

}
