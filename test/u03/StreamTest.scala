package u03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Streams.Stream
import u03.Streams.Stream._

class StreamTest {

  val s = Stream.take(Stream.iterate(0)(_+1))(10)



  @Test def testDrop(): Unit ={
    assertEquals(Stream.toList(cons(6, cons(7, cons(8, cons(9, empty()))))),
                  Stream.toList(Stream.drop(s)(6)))
    assertEquals(Stream.toList(empty()),
      Stream.toList(Stream.drop(empty())(6)))
  }

  @Test def testConstant(): Unit = {
    assertEquals(Stream.toList(Stream.take(constant("x"))(5)), Stream.toList(cons("x", cons("x",
      cons("x", cons("x", cons("x", empty())))))))
    assertEquals(Stream.toList(Stream.take(constant("x"))(2)),
      Stream.toList(cons("x", cons("x", empty()))))
  }

  @Test def testFibonacci(): Unit = {
    assertEquals(Stream.toList(cons(0, cons(1, cons(1, cons(2, cons(3, cons(5,
              cons(8, cons(13, empty()))))))))), Stream.toList(Stream.take(Stream.fibonacci[Int])(8)))

    assertEquals(Stream.toList(empty()), Stream.toList(Stream.take(Stream.fibonacci[Int])(0)))

    assertEquals(Stream.toList(cons(0, empty())), Stream.toList(Stream.take(Stream.fibonacci[Int])(1)))
  }
}
