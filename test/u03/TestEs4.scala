package u03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Lists.List
import u03.Lists.List.{Cons, Nil}

class TestEs4 {

  val lst = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))

  @Test def testFoldLeft() {
    assertEquals(16, List.foldLeft(lst)(0)(_+_))
    assertEquals(-16, List.foldLeft(lst)(0)(_-_))
  }

  @Test def testFoldRight() {
    assertEquals(16, List.foldRight(lst)(0)(_+_))
    assertEquals(-8, List.foldRight(lst)(0)(_-_))
  }
}
