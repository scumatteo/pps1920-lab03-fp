package u03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Lists.List._

class TestEs1 {

  val lst = Cons(10,Cons(20,Cons(30, Nil())))

  @Test def testDrop(): Unit ={
    assertEquals(Cons(20, Cons(30, Nil())), drop(lst, 1))
    assertEquals(Cons(30, Nil()), drop(lst, 2))
    assertEquals(Nil(), drop(lst, 5))
  }

  @Test def testFlatMap(): Unit ={
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), flatMap(lst)(v => Cons(v+1, Nil())))
    assertEquals(Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))), flatMap(lst)
                  (v => Cons(v+1, Cons(v+2, Nil()))))
  }

  @Test def testMapFromFlatMap(): Unit = {
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), mapFromFlatMap(lst)(v => v+1))
    assertEquals(Nil(), mapFromFlatMap(Nil[Int]())(v => v+1))
  }

  @ Test def testFilterFromFlatMap(): Unit = {
    assertEquals(Cons(20, Cons(30, Nil())), filterFromFlatMap(lst)(v => v >= 20))
    assertEquals(Cons(10, Nil()), filterFromFlatMap(lst)(v => v < 20))
    assertEquals(Nil(), filterFromFlatMap(Nil[Int]())(v => v < 20))
  }

}
