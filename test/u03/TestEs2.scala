package u03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Lists.List
import u02.Optionals.Option
import u03.Lists.List._
import u02.Optionals.Option._
import u03.es2.Es2

class TestEs2 {

  val lst = Cons(10, Cons(25, Cons(20, Nil())))

  @Test def testEs2(): Unit = {
    assertEquals(Some(25), Es2.maxWithTail(lst))
    assertEquals(None(), Es2.maxWithTail(Nil()))
  }

}
